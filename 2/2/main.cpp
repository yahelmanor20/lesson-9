#include <iostream>
#include <vector>
#include "member.h"
template<class T>
T compare(T t1, T t2)
{
	T sum = '0';
	if (sizeof(sum) != 1) sum = 0;
	return t1 == t2 ? sum : t1 > t2 ? -1 : 1;
}
template<class T>
void bubblesort(T arr[], int n)
{
	int c, d;
	T swap;
	for (c = 0; c < (n - 1); c++)
	{
		for (d = 0; d < n - c - 1; d++)
		{
			if (arr[d] > arr[d + 1]) /* For decreasing order use < */
			{
				swap = arr[d];
				arr[d] = arr[d + 1];
				arr[d + 1] = swap;
			}
		}
	}
}
template<class T>
void printArray(T arr[], int n)
{
	int c;
	bubblesort<T>(arr, n);
	for (c = 0; c < (n); c++)
	{
		std::cout << arr[c]<<std::endl;
	}
}
int main() {
	member a(1);
	member b(2);
	member c(3);
	member d(4);
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	std::cout << compare<char>('4', '4') << std::endl;
	std::cout << compare<member>(a, a) << std::endl;
	std::cout << compare<member>(a, d) << std::endl;

	//check bubblesort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 6;
	member doublearr[arr_size] = {c,d,a,b,c,d};
	bubblesort<member>(doublearr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doublearr[i] << " ";
	}
	std::cout << std::endl;

	char doubleArr[arr_size] = { '1', '2', '3', '1', '5'};
	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}