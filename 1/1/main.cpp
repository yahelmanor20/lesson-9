#pragma comment(lib, "printTreeToFile.lib")
#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include"printTreeToFile.h"
using namespace std;


int main()
{
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5"); 
	bs->insert("9");
	bs->insert("6");
	bs->insert("8");
	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(bs)+2 << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(bs) << endl;
	cout << "checks if 8 is on the tree: " << bs->search("8") << endl;
	//bs->printNodes();

	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	*bs->getRight()->getLeft() = *new BSNode("7");
	printTreeToFile(bs, textTree);
	bs->printNodes();

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;

	return 0;
}

