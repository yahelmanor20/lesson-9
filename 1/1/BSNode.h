#ifndef BSNode_H
#define BSNode_H

#include <algorithm>    // std::sort
#include <iostream>
#include <vector>
#include <string>
using namespace std;

class BSNode
{
public:
	BSNode(string data);
	BSNode(const BSNode& other);

	~BSNode();
	void DestroyRecursive(BSNode* node);

	void insert(string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;

	bool search(string val) const;

	int getHeight(int n = 0) const;
	int getDepth(const BSNode* root) const;

	void printNodes() ; //for question 1 part C
	void getTree(vector<BSNode*> *tmp, BSNode * root);
private:
	string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth
		
};

#endif