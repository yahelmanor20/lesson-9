#include "BSNode.h"
#include <vector>
#include <string>
BSNode::BSNode(string data)
{
	_data = data;
}
BSNode::BSNode(const BSNode & other) 
:_left(nullptr),
		_right(nullptr),
		_data(other._data)
	{
		if (other._left != nullptr)
		{
			_left = new BSNode(*other._left);
		}
		if (other._right != nullptr)
		{
			_right = new BSNode(*other._right);
		}
	}
BSNode::~BSNode()
{
	DestroyRecursive(this);
}
void BSNode::DestroyRecursive(BSNode* node)
{
	if (node)
	{
		DestroyRecursive(node->_left);
		DestroyRecursive(node->_right);
		node = NULL;
	}
}

//main function
void BSNode::insert(string value)
{
	bool shows = search(value);
	if (value <= _data)
	{
		if (this->_left == NULL)
		{
			this->_left = new BSNode(value);
			shows ? _left->_count++ : _count;
		}
		else _left->insert(value);
	}
	else if(value >= _data)
	{
		if (this->_right == NULL)
		{
			this->_right = new BSNode(value);
			shows ? _right->_count++ : _count;
		}
		else _right->insert(value);		
	}
}

BSNode & BSNode::operator=(const BSNode & other)
{
	_data = other._data;

	BSNode * left_orig = _left;
	_left = other._left != NULL ? new BSNode(*other._left) : NULL;
	delete left_orig;

	BSNode * right_orig = _right;
	_right = other._right != NULL?new BSNode(*other._right):NULL;
	delete right_orig;

	return *this;
}

//getters
bool BSNode::isLeaf() const
{
	if (_left == NULL && _right == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}

}
string BSNode::getData() const
{
	return this->_data;
}
BSNode * BSNode::getLeft() const
{
	return this->_left;
}
BSNode * BSNode::getRight() const
{
	return this->_right;
}

int BSNode::getCount() const
{
	return this->_count;
}

bool BSNode::search(string val) const
{
	if (this->getLeft() != NULL && getLeft()->search(val))
	{
		return true;
	}
	if (this->getRight() != NULL && getRight()->search(val))
	{
		return true;
	}
	return getData() == val ? true : false;
}

int BSNode::getHeight(int n) const
{
	if (this->getLeft() != NULL)
	{
		n++;
		return getLeft()->getHeight(n);
	}
	if (this->getRight() != NULL)
	{
		n++;
		return getRight()->getHeight(n);
	}
	if (this->isLeaf())
	{
		return n;
	}
	return 0;
}

int BSNode::getDepth(const BSNode * root) const
{
	if (root == NULL) 
        return 0; 
  
    // Base case : Leaf Node. This accounts for height = 1. 
    if (root->getLeft() == NULL && root->getRight() == NULL) 
    return 1; 
  
    // If getLeft() subtree is NULL, recur for getRight() subtree 
    if (!root->getLeft()) 
    return getDepth(root->getRight()) + 1; 
  
    // If getRight() subtree is NULL, recur for getLeft() subtree 
    if (!root->getRight()) 
    return getDepth(root->getLeft()) + 1; 
  
    return min(getDepth(root->getLeft()), getDepth(root->getRight())) + 1; 
}

void BSNode::printNodes()
{
	vector<BSNode*> sort;
	getTree(&sort,this);
	std::sort(sort.begin(), sort.end());
	for (int i = 0; i < sort.size(); i++)
	{
		cout << sort[i]->getData() << ' ' << sort[i]->getCount() << endl;
	}
}
//gets pointer of vector and  root and fill the vector with all the tree
void BSNode::getTree(vector<BSNode*> *tmp, BSNode * root)
{
	tmp->push_back(root);
	if (root->getLeft() != NULL)
	{
		getTree(tmp,root->getLeft());
	}
	if (root->getRight() != NULL)
	{
		getTree(tmp,root->getRight());
	}
}

